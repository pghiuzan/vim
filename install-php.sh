#!/bin/bash

sudo apt install software-properties-common
sudo add-apt-repository ppa:ondrej/php
sudo apt update
version="8.0"
sudo apt install "php$version" "php$version-dom" "php$version-curl" "php$version-mbstring php$version-bcmath php$version-redis"

# install composer
EXPECTED_CHECKSUM="$(php -r 'copy("https://composer.github.io/installer.sig", "php://stdout");')"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_CHECKSUM="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"

if [ "$EXPECTED_CHECKSUM" != "$ACTUAL_CHECKSUM" ]
then
    >&2 echo 'ERROR: Invalid installer checksum'
    rm composer-setup.php
    exit 1
fi

php composer-setup.php --quiet
RESULT=$?
rm composer-setup.php

if [ "$RESULT" -eq 0 ]; then
    sudo mv composer.phar /usr/local/bin/composer
fi

exit $RESULT
