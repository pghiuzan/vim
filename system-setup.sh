#!/bin/sh

set +x

# upgrade and update cache
sudo apt upgrade
sudo apt update

# install utilities
sudo apt install build-essential curl git exuberant-ctags neovim tmux jq silversearcher-ag make

# batery optimize
./install-tlp.sh

# nodejs
./install-nodejs.sh

mkdir -p ~/.config/nvim
rm -Rf ~/.config/nvim/plugged
rm -Rf ~/.local/share/nvim/site
rm -f ~/.config/nvim/init.vim
ln -s "$PWD/.vimrc" ~/.config/nvim/init.vim

# php
./install-php.sh

# docker
sudo apt install docker docker-compose
./move-docker-dir.sh

# git
./git-setup.sh

# misc
./misc.sh

# logout
gnome-session-quit
